﻿using UnityEngine;

public class Brick : MonoBehaviour
{
	public Vector2 PositionInLevelArray { get; set; }
	public EventInfo<Brick> BrickCollisionInfo { get; private set; }

	void Awake()
	{
		BrickCollisionInfo = new EventInfo<Brick> {Parameter = this};
	}
}
