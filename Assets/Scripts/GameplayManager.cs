﻿using System;
using System.Collections;
using System.Linq;
using UnityEngine;

public class GameplayManager : MonoBehaviour
{
	public static GameplayManager Instance { get; private set; }

	public int CurrentScore { get; private set; }
	[SerializeField] private int scoreForBrick;
	[SerializeField] public Brick[] CurrentBrickArray { get; private set; }
	[SerializeField] public int[,] CurrentLevelArray { get; private set; }

	[SerializeField] private LevelGenerator levelGenerator;
	[SerializeField] private Ball ball;
	[SerializeField] private PaddleController paddleController;

	

	[Header("Game balance stats - lazy")]
	[SerializeField] private float slowMoAmount, slowMoDuration;

	void Awake()
	{
		if (Instance != null && Instance != this)
			Destroy(gameObject);
		Instance = this;

		EventDispatcher.Instance.AddListener(x => OnBrickAndBallColision((EventInfo<Brick>)x), Message.BallAndBrickColliding);
	}

	void Start()
	{
		StartNewLevel();
	}

	void StartNewLevel()
	{
		CurrentLevelArray = levelGenerator.GenerateLevel();
		CurrentBrickArray = levelGenerator.DrawLevel().ToArray();

		EventDispatcher.Instance.RaiseEvent(Message.LevelCreated, EventDispatcher.Instance.EmptyInfo);
	}

	void OnBrickAndBallColision(EventInfo<Brick> info)
	{
		if (info.Parameter == null)
		{
			Debug.LogError("Non existent brick colliding");
			return;
		}
		var xPos = (int) info.Parameter.PositionInLevelArray.x;
		var yPos = (int) info.Parameter.PositionInLevelArray.y;

		int scoreMultiplier = 1;

		switch ((BrickType)CurrentLevelArray[xPos,yPos])
		{
			case BrickType.Empty:
				Debug.Log("Empty");
				break;
			case BrickType.NormalBrick:
				Debug.Log("Normal");
				break;
			case BrickType.DoublePointsBrick:
				scoreMultiplier = 2;
				Debug.Log("Double");
				break;
			case BrickType.TimeSlowingBrick:
				StartCoroutine(SlowTime(slowMoDuration));
				Debug.Log("TimeSlowing");
				break;
			case BrickType.LifeGainBrick:
				Debug.Log("LifeGain");
				break;
		}

		CurrentLevelArray[xPos, yPos] = 0;
		//Destroy(info.Parameter.gameObject);
		info.Parameter.gameObject.SetActive(false);
		CurrentScore += scoreForBrick * scoreMultiplier;
	}

	IEnumerator SlowTime(float duration)
	{
		var timeScaleWhenTriggered = Time.timeScale;

		Time.timeScale *= slowMoAmount;
		Debug.Log("Starting scale: " + Time.timeScale);

		yield return new WaitForSeconds(duration);

		Time.timeScale /= slowMoAmount;
		Debug.Log("Ending scale: " + Time.timeScale);
	}

}
