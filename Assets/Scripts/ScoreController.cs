﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreController : MonoBehaviour
{
	public int CurrentScore; /*{ get; private set; }*/
	[SerializeField] private int pointsPerBrick;

	void Awake()
	{
		EventDispatcher.Instance.AddListener(x => AddScore((EventInfo<Brick>)x), Message.BallAndBrickColliding);
	}

	void AddScore(EventInfo<Brick> collisionInfo)
	{
		if (GameplayManager.Instance.CurrentLevelArray[(int) collisionInfo.Parameter.PositionInLevelArray.x,
			    (int) collisionInfo.Parameter.PositionInLevelArray.y] == (int) BrickType.DoublePointsBrick)
		{
			CurrentScore += 2 * pointsPerBrick;
		}
		else
		{
			CurrentScore += pointsPerBrick;
		}
	}

	// Update is called once per frame
	void Update () {
		
	}
}
