﻿using System;

[Serializable]
public class LevelSnapshot
{
	public int CurrentLevel;
	public int[,] LevelArray;
}
