﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VR.WSA.Persistence;

public class Singleton<T> : MonoBehaviour
	where T : class, new()
{
	private static T instance;

	public static T Instance
	{
		get { return instance ?? (instance = new T()); }
	}
}
