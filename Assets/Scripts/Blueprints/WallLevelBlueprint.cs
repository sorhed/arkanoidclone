﻿using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using UnityEngine;

public class WallLevelBlueprint : MonoBehaviour, ILevelBlueprint
{
	//zero bricks excluded
	[SerializeField] private int numberOfBricks, numOfT1Bricks, numOfT2Bricks, numOfT3Bricks, numOfT4Bricks;
	[SerializeField] private List<int> brickList;

	[ExecuteInEditMode]
	public void Init()
	{
		brickList = new List<int>();

		brickList.AddRange(Enumerable.Repeat(1, numOfT1Bricks));
		brickList.AddRange(Enumerable.Repeat(2, numOfT2Bricks));
		brickList.AddRange(Enumerable.Repeat(3, numOfT3Bricks));
		brickList.AddRange(Enumerable.Repeat(4, numOfT4Bricks));

		brickList.Shuffle();
	}

	[NotNull]
	[ExecuteInEditMode]
	public int[,] GenerateLevel()
	{
		var res = new int[13, 9];
		for (int x = 0; x < 13; x++)
		{
			for (int y = 0; y < 9; y++)
			{
				if (y < 5) res[x, y] = (int)BrickType.Empty;
				else
				{
					res[x, y] = brickList[0];
					brickList.RemoveAt(0);
				}
			}
		}
		return res;
	}
}
