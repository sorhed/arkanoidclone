﻿using System.Collections.Generic;
using UnityEngine;

public enum BrickType
{
	Empty,
	NormalBrick,
	DoublePointsBrick,
	TimeSlowingBrick,
	LifeGainBrick
}

public enum LevelType
{
	Wall,
}

public interface ILevelBlueprint
{
	void Init();
	int[,] GenerateLevel();
}

public struct Location
{
	public int x;
	public int y;
}


public class LevelGenerator : MonoBehaviour
{
	public Vector2 BrickSize;
	private ILevelBlueprint[] levelBlueprints;
	private int margin;
	private int[,] LevelArray;
	private Dictionary<BrickType, Transform> brickDictionary;
	private bool isGenerated;
	[SerializeField] private Transform brickT1, brickT2, brickT3, brickT4;
	[SerializeField] private Transform holder;

	void Awake()
	{
		brickDictionary = new Dictionary<BrickType, Transform>
		{
			{BrickType.NormalBrick, brickT1},
			{BrickType.DoublePointsBrick, brickT2},
			{BrickType.TimeSlowingBrick, brickT3},
			{BrickType.LifeGainBrick, brickT4}
		};

		levelBlueprints = new[] { FindObjectOfType<WallLevelBlueprint>() };

		margin = 89;
	}

	[ExecuteInEditMode]
	public void Init()
	{
		brickDictionary = new Dictionary<BrickType, Transform>
		{
			{BrickType.NormalBrick, brickT1},
			{BrickType.DoublePointsBrick, brickT2},
			{BrickType.TimeSlowingBrick, brickT3},
			{BrickType.LifeGainBrick, brickT4}
		};
		margin = 89;
		levelBlueprints[0].Init();
	}

	[ExecuteInEditMode]
	public int[,] GenerateLevel(int[,] snapshotArray = null, LevelType levelType = LevelType.Wall)
	{
		levelBlueprints[0].Init();

		if (snapshotArray != null)
		{
			LevelArray = snapshotArray;
		}
		LevelArray = levelBlueprints[(int)levelType].GenerateLevel();

		isGenerated = true;

		return LevelArray;
	}

	[ExecuteInEditMode]
	public IEnumerable<Brick> DrawLevel()
	{
		var currentPos = new Vector2(margin + BrickSize.x / 2, 1080 - BrickSize.y / 2);
		for (int y = 0; y < LevelArray.GetLength(1); y++)
		{
			for (int x = 0; x < LevelArray.GetLength(0); x++)
			{
				var brick = LevelArray[x, y];
				//Debug.Log("x: " + x);
				//Debug.Log("y: " + y);
				//Debug.Log("CurrentPos: " + currentPos);
				if (brick != 0)
				{
					var i = Instantiate(brickDictionary[(BrickType)brick], currentPos, Quaternion.identity);
					i.SetParent(holder, false);
					var b = i.gameObject.GetComponent<Brick>();
					if (b != null)
					{
						b.PositionInLevelArray = new Vector2(x, y);
						yield return b;
					}
					else
					{
						Debug.LogError("Brick script not attached to the prefab");
					}
				}
				if (x == LevelArray.GetLength(0) - 1)
				{
					var newPos = new Vector2(margin + BrickSize.x / 2, currentPos.y - BrickSize.y);
					currentPos = newPos;
				}
				else
				{
					currentPos += new Vector2(BrickSize.x, 0);
				}
			}
		}
	}

	public void OnExitLevel()
	{
		LevelArray = null;
		isGenerated = false;
	}

	//[ExecuteInEditMode]
	//public void GenerateWallLevel()
	//{
	//	BrickList.Shuffle();

	//	for (int x = 0; x < 12; x++)
	//		{
	//			for (int y = 0; y < 9; y++)
	//			{
	//				if (y < 5) LevelArray[x, y] = (int) BrickType.Empty;
	//				else
	//				{
	//					LevelArray[x, y] = BrickList[0];
	//					BrickList.RemoveAt(0);
	//				}
	//				Debug.Log(LevelArray[x,y]);
	//			}
	//		}
	//}

}
