﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Extensions
{

	private static readonly System.Random rng = new System.Random();

	public static void Shuffle<T>(this IList<T> list)
	{
		int n = list.Count;
		while (n > 1)
		{
			n--;
			int k = rng.Next(n + 1);
			T value = list[k];
			list[k] = list[n];
			list[n] = value;
		}
	}

	//public static T GetHolderOfType<T>(this IDataHolder[] source)
	//{
	//	return source.OfType<T>().Select(x => x).FirstOrDefault();
	//}

	//public static T GetManagerOfType<T>(this IManager[] source)
	//{
	//	return source.OfType<T>().Select(x => x).FirstOrDefault();
	//}

}
