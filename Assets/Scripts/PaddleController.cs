﻿using UnityEngine;

[RequireComponent(typeof(BoxCollider2D), typeof(Rigidbody2D), typeof(SpriteRenderer))]
public class PaddleController : MonoBehaviour
{

	[SerializeField] private float speed;
	public float Speed
	{
		get { return speed; }
	}

	private Vector3 startingPos;

	void Awake()
	{
		startingPos = transform.position;
	}

	void LevelInit()
	{
		transform.position = startingPos;
	}

	// Update is called once per frame
	void Update()
	{
		if (Input.GetKey(KeyCode.A))
		{
			transform.position += Vector3.left * speed * Time.deltaTime;
		}

		if (Input.GetKey(KeyCode.D))
		{
			transform.position += Vector3.right * speed * Time.deltaTime;
		}


	}
}