﻿using System;
using System.Collections.Generic;
using UnityEngine;

public enum Message
{
	PaddleAndBallColliding,
	BallAndBrickColliding,
	LevelCreated,
}

public class EventInfo<T> : IEventInfo
{
	public T Parameter;
}

public class EventInfo<T, Y> : IEventInfo
{
	public T Parameter1;
	public Y Parameter2;
}

public class EventInfo<T, Y, Z> : IEventInfo
{
	public T Parameter1;
	public Y Parameter2;
	public Z Parameter3;
}

public interface IEventInfo
{
}

public class EmptyInfo : IEventInfo
{
}


public class EventDispatcher : Singleton<EventDispatcher>
{
	public readonly IEventInfo EmptyInfo = new EmptyInfo();
	private readonly Dictionary<Message, List<Action<IEventInfo>>> eventMapper = new Dictionary<Message, List<Action<IEventInfo>>>();

	public void AddListener(Action<IEventInfo> invokedOnEvent, Message mappedMessage)
	{
		List<Action<IEventInfo>> l;
		if (eventMapper.TryGetValue(mappedMessage, out l))
			l.Add(invokedOnEvent);
		else
			eventMapper.Add(mappedMessage, new List<Action<IEventInfo>> { invokedOnEvent });
	}

	public void RemoveListener(Action<IEventInfo> invokedOnEvent, Message mappedMessage)
	{
		List<Action<IEventInfo>> l;
		if (eventMapper.TryGetValue(mappedMessage, out l))
			l.Remove(invokedOnEvent);
		else
			Debug.LogWarning("Listener not found - redundad method call");

	}

	public void RaiseEvent(Message message, IEventInfo eventInfo)
	{
		List<Action<IEventInfo>> l;
		if (eventMapper.TryGetValue(message, out l))
			l.ForEach(a => a.Invoke(eventInfo));
	}

}