﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(BoxCollider2D), typeof(Rigidbody2D), typeof(SpriteRenderer))]
public class Ball : MonoBehaviour
{
	[SerializeField] private float speed;
	public float Speed
	{
		get { return speed; }
	}

	private Vector3 startingPosition;
	private PaddleController paddleController;
	private Rigidbody2D thisRigidbody;
	[SerializeField]private Brick[] brickArray;
	[SerializeField] private float angularVelocity;

	void Awake()
	{
		thisRigidbody = GetComponent<Rigidbody2D>();
		paddleController = FindObjectOfType<PaddleController>();
		startingPosition = transform.position;
		EventDispatcher.Instance.AddListener(empty => brickArray = GameplayManager.Instance.CurrentBrickArray, Message.LevelCreated);
		//StartTheBall();
	}

	void Start()
	{
		EventDispatcher.Instance.RemoveListener(empty => brickArray = GameplayManager.Instance.CurrentBrickArray, Message.LevelCreated);
		LevelInit();
	}

	void LevelInit()
	{
		transform.position = startingPosition;
		transform.SetParent(paddleController.transform);
	}

	public void StartTheBall()
	{
		transform.parent = null;
		thisRigidbody.velocity = new Vector2(0.5f, 1) * speed;
		thisRigidbody.angularVelocity = angularVelocity;
	}

	void Update()
	{
		if (transform.parent != null && Input.GetKeyUp(KeyCode.Space))
			StartTheBall();
	}

	void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.collider.CompareTag("Brick"))
		{
			EventDispatcher.Instance.RaiseEvent(Message.BallAndBrickColliding,
				brickArray.Where(brick => ReferenceEquals(brick.gameObject, collision.collider.gameObject))
					.Select(brick => brick.BrickCollisionInfo).FirstOrDefault());
		}
		//if (collision.collider.CompareTag("BottomBoundry"))
		//{
		//	Debug.Log("BottomBoundry Collsion");
		//}
		//if (collision.collider.CompareTag("Paddle"))
		//{
		//	Debug.Log("PaddleCollision");
		//}
	}

	// Update is called once per frame
}
